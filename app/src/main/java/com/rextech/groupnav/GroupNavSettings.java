package com.rextech.groupnav;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class GroupNavSettings extends ActionBarActivity {

    private EditText groupNameInput;

    private EditText groupTargetInput;

    private EditText addMemberInput;

    private Button addMemberButton;

    private LinearLayout membersList;

    private Button submit;

    private List<String> members;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_nav_settings);

        members = new ArrayList<String>();

        groupNameInput = (EditText) findViewById(R.id.groupName);
        groupTargetInput = (EditText) findViewById(R.id.groupTarget);
        addMemberInput = (EditText) findViewById(R.id.addMemberInput);
        addMemberButton = (Button) findViewById(R.id.addMemberButton);
        membersList = (LinearLayout) findViewById(R.id.list);
        submit = (Button) findViewById(R.id.submitGroup);

        addMemberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView member = new TextView(getApplicationContext());
                if (TextUtils.isEmpty(addMemberInput.getText())) {
                    addMemberInput.setError("Необходимо заполнить это поле");
                } else {
                    member.setText(addMemberInput.getText());
                    membersList.addView(member);
                    members.add(addMemberInput.getText().toString());
                    addMemberInput.setText("");
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(groupNameInput.getText())) {
                    groupNameInput.setError("Необходимо заполнить!");
                    return;
                }
                if (TextUtils.isEmpty(groupTargetInput.getText())) {
                    groupTargetInput.setError("Необходимо заполнить!");
                    return;
                }
                if (members.isEmpty()) {
                    addMemberInput.setError("Необходимо указать членов группы!");
                    return;
                }
                JSONObject message = new JSONObject();
                try {
                    message
                            .put("operation", "newGroup")
                            .put("name", groupNameInput.getText().toString())
                            .put("target", groupTargetInput.getText().toString())
                            .put("members", new JSONArray(members));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent sendIntent = new Intent(getString(R.string.network_service_send));
                sendIntent.putExtra("payload", message.toString());
                sendBroadcast(sendIntent);

                Intent activityIntent = new Intent(getApplicationContext(), GroupNav.class);
                activityIntent.putExtra("name", groupNameInput.getText().toString());
                activityIntent.putExtra("membersCount", members.size());
                for (int i = 0; i < members.size(); i++) {
                    activityIntent.putExtra("member" + i, members.get(i));
                }
                activityIntent.putExtra("target", groupTargetInput.getText().toString());
                startActivity(activityIntent);

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_group_nav_settings, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
