package com.rextech.groupnav;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class NavSettings extends ActionBarActivity implements View.OnClickListener {

    private EditText startInForm;

    private EditText finishInForm;

    private Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_settings);

        startInForm = (EditText) findViewById(R.id.start);
        finishInForm = (EditText) findViewById(R.id.finish);
        submit = (Button) findViewById(R.id.submitGroup);

        submit.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nav_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        String start = startInForm.getText().toString();
        String end = finishInForm.getText().toString();
        Intent intent = new Intent(getApplicationContext(), SelfNavigation.class);
        intent.putExtra("start", start);
        intent.putExtra("end", end);
        startActivity(intent);
    }
}
