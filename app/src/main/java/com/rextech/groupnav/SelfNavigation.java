package com.rextech.groupnav;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.location.Location;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tyczj.mapnavigator.Directions;
import com.tyczj.mapnavigator.Legs;
import com.tyczj.mapnavigator.Navigator;
import com.tyczj.mapnavigator.Route;
import com.tyczj.mapnavigator.Steps;

import java.text.DateFormat;
import java.util.Date;
import java.util.Iterator;



public class SelfNavigation extends FragmentActivity
        implements
        OnMapReadyCallback,
        Navigator.OnPathSetListener,
        LocationListener {

    private final float ZOOM = 20f;
    private final float TILT = 85f;
    private final int ANIM_DURATION = 1500;

    private GoogleMap mMap;
    private MapFragment mMapFragment;
    private String startAddress;
    private String endAddress;
    private Intent mIntent;
    private Navigator mNav;
    private Marker trackingMarker;

    private TextView instructions;

    private Iterator<Steps> iterator;

    private Steps currStep;

    private BroadcastReceiver recieverNetwork;

    private BroadcastReceiver recieverLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mIntent = getIntent();

        mMapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);

        mMapFragment.getMapAsync(this);

        startAddress = mIntent.getStringExtra("start");
        endAddress = mIntent.getStringExtra("end");

        instructions = (TextView) findViewById(R.id.instructions);

        IntentFilter filterNetwork = new IntentFilter(getString(R.string.network_service_recieve));
        recieverNetwork = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

            }
        };
        registerReceiver(recieverNetwork, filterNetwork);

        IntentFilter filterLocation = new IntentFilter("LOCATION_RECIEVE");
        recieverLocation = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                onLocationChanged(convertLatLngToLocation(new LatLng(
                        intent.getDoubleExtra("longitude", 0.0),
                        intent.getDoubleExtra("latitude", 0.0)
                )));


            }
        };

        registerReceiver(recieverLocation, filterLocation);

    }



    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.mMap = googleMap;


        mNav = new Navigator(mMap, startAddress, endAddress);
        mNav.setPathLineWidth(8);
        mNav.setOnPathSetListener(this);
        mNav.setPathColor(Color.BLUE, Color.BLUE, Color.BLUE);
        mNav.findDirectionsByString(false);




    }


    @Override
    public void onPathSetListener(Directions directions) {

        iterator = directions
                .getRoutes()
                .get(0)
                .getLegs()
                .get(0)
                .getSteps()
                .iterator();

        currStep = directions
                .getRoutes()
                .get(0)
                .getLegs()
                .get(0)
                .getSteps()
                .get(0);



        Log.d("Directions", "routes.size is " + directions.getRoutes().size());
        if (directions.getRoutes().get(0) != null) {
            Log.d("Directions", "routes.get(0) isn't null");
        } else {
            Log.d("Directions", "routes.get(0) is" + " null");
        }

        Route currRoute = directions.getRoutes().get(0);
        Legs startLeg = currRoute.getLegs().get(0);
        Steps startStep = startLeg.getSteps().get(0);
        if (startLeg != null && startStep != null) {
            Log.d("Directions", "startLeg and start step isn't null");
            Log.d("Directions", "legs.size = " + currRoute.getLegs().size());
        } else {
            Log.d("Directions", "startLeg and start step is null");
        }
        CameraPosition position = new CameraPosition(startStep.getStepStartPosition(),
                ZOOM,
                TILT,
                bearingBetweenLatLngs(startStep.getStepStartPosition(), startStep.getEndStepPosition()));
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(position));
        trackingMarker = mMap.addMarker(new MarkerOptions()
                        .position(startStep.getStepStartPosition())
                        .flat(true)
                        .icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.ic_navigation))
                        .draggable(false)
        );

    }

    private Location convertLatLngToLocation(LatLng latLng) {
        Location location = new Location("location");
        location.setLongitude(latLng.longitude);
        location.setLatitude(latLng.latitude);
        return location;

    }

    private float bearingBetweenLatLngs(LatLng beginLatLng,LatLng endLatLng) {
        Location beginLocation = convertLatLngToLocation(beginLatLng);
        Location endLocation = convertLatLngToLocation(endLatLng);
        return beginLocation.bearingTo(endLocation);
    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng currLatLng = new LatLng(location.getLatitude(), location.getLongitude());

        animateMarker(trackingMarker, currLatLng, false);



    }

    public void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hidingMarker) {
        final LatLng startPosition = marker.getPosition();
        final LatLng finalPosition = toPosition;
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final Interpolator interpolator = new AccelerateDecelerateInterpolator();
        final float durationInMs = 3000;
        final boolean hideMarker = hidingMarker;
        marker.setRotation(bearingBetweenLatLngs(startPosition, finalPosition));

        handler.post(new Runnable() {
            long elapsed;
            float t;
            float v;

            @Override
            public void run() {
                // Calculate progress using interpolator
                elapsed = SystemClock.uptimeMillis() - start;
                t = elapsed / durationInMs;
                v = interpolator.getInterpolation(t);

                LatLng currentPosition = new LatLng(
                        startPosition.latitude*(1-t)+finalPosition.latitude*t,
                        startPosition.longitude*(1-t)+finalPosition.longitude*t);


                marker.setPosition(currentPosition);

                // Repeat till progress is complete.
                if (t < 1) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }

}
