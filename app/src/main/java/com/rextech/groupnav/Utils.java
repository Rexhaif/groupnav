package com.rextech.groupnav;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.SystemClock;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONObject;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by Даниил on 25.04.2015.
 */
public class Utils {

    public static void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hidingMarker) {
        final LatLng startPosition = marker.getPosition();
        final LatLng finalPosition = toPosition;
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final Interpolator interpolator = new AccelerateDecelerateInterpolator();
        final float durationInMs = 3000;
        final boolean hideMarker = hidingMarker;
        marker.setRotation(bearingBetweenLatLngs(startPosition, finalPosition));

        handler.post(new Runnable() {
            long elapsed;
            float t;
            float v;

            @Override
            public void run() {
                // Calculate progress using interpolator
                elapsed = SystemClock.uptimeMillis() - start;
                t = elapsed / durationInMs;
                v = interpolator.getInterpolation(t);

                LatLng currentPosition = new LatLng(
                        startPosition.latitude*(1-t)+finalPosition.latitude*t,
                        startPosition.longitude*(1-t)+finalPosition.longitude*t);


                marker.setPosition(currentPosition);

                // Repeat till progress is complete.
                if (t < 1) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }

    public static Location convertLatLngToLocation(LatLng latLng) {
        Location location = new Location("location");
        location.setLongitude(latLng.longitude);
        location.setLatitude(latLng.latitude);
        return location;

    }

    public static float bearingBetweenLatLngs(LatLng beginLatLng,LatLng endLatLng) {
        Location beginLocation = convertLatLngToLocation(beginLatLng);
        Location endLocation = convertLatLngToLocation(endLatLng);
        return beginLocation.bearingTo(endLocation);
    }

    public static boolean checkKeys(JSONObject msg, String... keys) {

        Set<String> keySet = new HashSet<>();
        Iterator<String> iterator = msg.keys();

        while (iterator.hasNext()){
            keySet.add(iterator.next());
        }

        for (String key : keys) {
            if ((!keySet.contains(key))) return false;
            else if (msg.isNull(key)) return false;
        }
        return true;
    }

    public static void send(JSONObject message, Context context) {
        Intent intent = new Intent(context.getString(R.string.network_service_send));
        intent.putExtra("payload", message.toString());
        context.sendBroadcast(intent);
    }



}
