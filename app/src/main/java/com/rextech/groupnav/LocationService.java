package com.rextech.groupnav;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

public class LocationService extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener{

    private GoogleApiClient mGoogleApiClient;

    private Location mLastLocation;

    private LocationRequest mLocationRequest;

    private int connSuspendCounter = 0;

    public final String LOCATION_RECIEVE_ACTION = "LOCATION_RECIEVE";




    public LocationService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        buildGoogleApiClient();
        mGoogleApiClient.connect();

        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        startLocationUpdates();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(4000);
        mLocationRequest.setFastestInterval(4000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {
        createLocationRequest();
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (connSuspendCounter > 25){
            try {
                throw new Exception("Can not connect, connection suspend");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        connSuspendCounter++;
        Log.d("LocationService", "Connection suspended, passed " + connSuspendCounter + ", tryings,  retrieving..");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        try {
            throw new Exception(connectionResult.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("Location Service", location.getLatitude()
                + ", "
                + location.getLongitude());
        Intent intent = new Intent(LOCATION_RECIEVE_ACTION);
        intent.putExtra("latitude", location.getLongitude());
        intent.putExtra("longitude", location.getLatitude());
        sendBroadcast(intent);
    }
}
