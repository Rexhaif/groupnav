package com.rextech.groupnav;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Button;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;


public class MainMap extends ActionBarActivity implements OnMapReadyCallback, LocationListener {

    public final String LOCATION_RECIEVE_ACTION = "LOCATION_RECIEVE";

    private Button selfNavigation;

    private Button groupNavigation;

    private MapFragment mMapFragment;

    private BroadcastReceiver locationReciever;

    private BroadcastReceiver networkReciever;

    private IntentFilter filter;

    private Marker tracker;

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_map);



        mMapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.fragment);
        mMapFragment.getMapAsync(this);

        selfNavigation = (Button) findViewById(R.id.button);
        groupNavigation = (Button) findViewById(R.id.button2);
        groupNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), GroupNavSettings.class));
            }
        });
        selfNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), NavSettings.class));
            }
        });
        filter = new IntentFilter(LOCATION_RECIEVE_ACTION);



        locationReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                onLocationChanged(convertLatLngToLocation(new LatLng(
                        intent.getDoubleExtra("longitude", 0.0),
                        intent.getDoubleExtra("latitude", 0.0)
                )));
                Log.d("Location Reciever", "" + intent.getDoubleExtra("longitude", 0.0)
                        + intent.getDoubleExtra("latitude", 0.0) );
            }
        };

        registerReceiver(locationReciever, filter);
        startService(new Intent(this, LocationService.class ));

        IntentFilter networkFilter = new IntentFilter(getString(R.string.network_service_recieve));
        networkReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                JSONObject message = new JSONObject();
                try {
                    message = new JSONObject(intent.getStringExtra("payload"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (Utils.checkKeys(message, "from", "operation", "target")) {
                    onGroupInvite(message);
                }
            }
        };
        registerReceiver(networkReciever, networkFilter);

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMapType(googleMap.MAP_TYPE_HYBRID);
        tracker = googleMap.addMarker(new MarkerOptions().position(new LatLng(0.0, 0.0)));
        mMap = googleMap;
    }

    @Override
    public void onLocationChanged(Location location) {
        animateMarker(tracker,
                new LatLng(location.getLatitude(), location.getLongitude()),
                false);
    }

    private Location convertLatLngToLocation(LatLng latLng) {
        Location location = new Location("location");
        location.setLongitude(latLng.longitude);
        location.setLatitude(latLng.latitude);
        return location;

    }

    private float bearingBetweenLatLngs(LatLng beginLatLng,LatLng endLatLng) {
        Location beginLocation = convertLatLngToLocation(beginLatLng);
        Location endLocation = convertLatLngToLocation(endLatLng);
        return beginLocation.bearingTo(endLocation);
    }

    public void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hidingMarker) {
        final LatLng startPosition = marker.getPosition();
        final LatLng finalPosition = toPosition;
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final Interpolator interpolator = new AccelerateDecelerateInterpolator();
        final float durationInMs = 3000;
        final boolean hideMarker = hidingMarker;
        marker.setRotation(bearingBetweenLatLngs(startPosition, finalPosition));

        handler.post(new Runnable() {
            long elapsed;
            float t;
            float v;

            @Override
            public void run() {
                // Calculate progress using interpolator
                elapsed = SystemClock.uptimeMillis() - start;
                t = elapsed / durationInMs;
                v = interpolator.getInterpolation(t);

                LatLng currentPosition = new LatLng(
                        startPosition.latitude*(1-t)+finalPosition.latitude*t,
                        startPosition.longitude*(1-t)+finalPosition.longitude*t);


                marker.setPosition(currentPosition);

                // Repeat till progress is complete.
                if (t < 1) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }



    private void onGroupInvite(final JSONObject message) {
        try {
            AlertDialog alert = new AlertDialog.Builder(getApplicationContext())
                    .setTitle("Приглашение в группу")
                    .setMessage("Пользователь "
                            + message.getString("from")
                            + " пригласил вас в группу. Пункт назначения: "
                            + message.getString("target"))
                    .setPositiveButton("Присоедениться", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                send(new
                                        JSONObject()
                                            .put("name", message.getString("name"))
                                            .put("operation","groupMsg")
                                            .put("groupOperation", "inviteResponse")
                                            .put("parameter", "accept")
                                );

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Intent activityIntent = new Intent(getApplicationContext(), GroupNav.class);
                            try {
                                activityIntent.putExtra("name", message.getString("name"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                activityIntent.putExtra("membersCount", message.getJSONArray("members").length());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                for (int i = 0; i < message.getJSONArray("members").length(); i++) {
                                    activityIntent.putExtra("member" + i, message.getJSONArray("members").getString(i));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                activityIntent.putExtra("target", message.getString("target"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            startActivity(activityIntent);
                        }
                    })
                    .setNegativeButton("Отказаться", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .create();
            alert.show();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void send(JSONObject message) {
        Intent intent = new Intent(getString(R.string.network_service_send));
        intent.putExtra("payload", message.toString());
        sendBroadcast(intent);
    }

}
