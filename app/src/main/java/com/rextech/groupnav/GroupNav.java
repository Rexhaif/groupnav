package com.rextech.groupnav;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tyczj.mapnavigator.Directions;
import com.tyczj.mapnavigator.Legs;
import com.tyczj.mapnavigator.Navigator;
import com.tyczj.mapnavigator.Route;
import com.tyczj.mapnavigator.Steps;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class GroupNav extends ActionBarActivity implements
        OnMapReadyCallback,
        LocationListener,
        Navigator.OnPathSetListener{

    private final float ZOOM = 20f;
    private final float TILT = 85f;

    private final LatLng defaultPos = new LatLng(0.0, 0.0);

    private GoogleMap map;

    private Button navStart;

    private BroadcastReceiver recieverNetwork;

    private BroadcastReceiver recieverLocation;

    private Navigator mNav;

    private Marker mMarker;

    private Map<String, Marker> members;

    private String groupName;

    private String groupTarget;

    private Intent settingsIntent;

    private boolean recieveLocationUpdates = false;
    private Iterator<Steps> iterator;
    private Steps currStep;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_nav);

        startService(new Intent(getApplicationContext(), LocationService.class));

        navStart = (Button) findViewById(R.id.navStart);
        settingsIntent = getIntent();

        members = new HashMap<>();

        groupName = settingsIntent.getStringExtra("name");
        groupTarget = settingsIntent.getStringExtra("target");



        IntentFilter filterNetwork = new IntentFilter(getString(R.string.network_service_recieve));
        recieverNetwork = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                JSONObject message = new JSONObject();
                try {
                    message = new JSONObject(intent.getStringExtra("payload"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    String operation = message.getString("operation");
                    if (operation.equals("updateCoords")) {
                        JSONArray coords = message.getJSONArray("coords");
                        animateMarker(members.get(message.getString("from")),
                                new LatLng(coords.getDouble(0), coords.getDouble(1)),
                                false);
                    } else if (operation.equals("message")) {
                        Toast.makeText(getApplicationContext(),
                                        "От " + message.getString("from")
                                        + ": " + message.getString("text"),
                                Toast.LENGTH_LONG
                        ).show();
                    } else if (operation.equals("leave")){
                        Toast.makeText(getApplicationContext(),
                                "" + message.getString("from") + " вышел из группы."
                                        + " " + message.getString("reason"),
                                Toast.LENGTH_LONG
                        ).show();
                        members.get(message.getString("from")).remove();
                        members.remove(message.getString("from"));

                    } else if (operation.equals("groupDestroy")) {
                        Toast.makeText(getApplicationContext(),
                                message.getString("from") +
                                " расформировал группу", Toast.LENGTH_LONG)
                                .show();
                        unregisterReceiver(this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        registerReceiver(recieverNetwork, filterNetwork);

        IntentFilter filterLocation = new IntentFilter("LOCATION_RECIEVE");
        recieverLocation = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    send(new JSONObject()
                    .put("operation", "groupMsg")
                    .put("name", groupName)
                    .put("groupOperation", "updateCoords")
                    .put("parameter", new JSONArray()
                            .put(intent.getDoubleExtra("longitude", 0.0))
                            .put(intent.getDoubleExtra("latitude", 0.0))));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (recieveLocationUpdates){
                    onLocationChanged(convertLatLngToLocation(new LatLng(
                            intent.getDoubleExtra("longitude", 0.0),
                            intent.getDoubleExtra("latitude", 0.0)

                    )));
                }
            }
        };
        registerReceiver(recieverLocation, filterLocation);



        MapFragment fragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapGroup);
        fragment.getMapAsync(this);


    }


    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_group_nav, menu);
        menu.add("Написать сообщение").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                final EditText messageInput = new EditText(getApplicationContext());

                AlertDialog dialog = new AlertDialog.Builder(getApplicationContext())
                        .setTitle("Написать сообщение")
                        .setMessage("Напишите сообщение всем участникам группы")
                        .setView(messageInput)
                        .setPositiveButton("Отправить", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(getString(R.string.network_service_send));
                                JSONObject message = new JSONObject();
                                try {
                                    message
                                    .put("operation", "groupMsg")
                                    .put("name", groupName)
                                    .put("groupOperation", "message")
                                    .put("parameter", messageInput.getText().toString());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                intent.putExtra("payload", message.toString());
                                Log.d("GroupNavActivity", "Пользователь написал сообщение");
                                sendBroadcast(intent);
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();

                return false;
            }
        });
        menu.add("Покинуть группу").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent intent = new Intent(getString(R.string.network_service_send));
                JSONObject message = new JSONObject();
                try {
                    message
                            .put("operation", "groupMsg")
                            .put("name", groupName)
                            .put("groupOperation", "leave")
                            .put("parameter", "Я устал, я ухожу!");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                intent.putExtra("payload", message.toString());
                Log.d("GroupNavActivity", "Пользователь покинул группу");
                sendBroadcast(intent);
                startActivity(new Intent(getApplicationContext(), MainMap.class));
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.map = googleMap;
        recieveLocationUpdates = true;

        for (int i = 1; i < settingsIntent.getIntExtra("membersCount", 1); ++i) {
            members
                    .put(settingsIntent
                                    .getStringExtra("member" + i),
                            map
                                    .addMarker(new MarkerOptions()
                                    .position(defaultPos)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_black_36dp))
                                    .flat(true)));

        }






    }

    @Override
    public void onLocationChanged(Location location) {
        mNav = new Navigator(map,
                "" + location.getLatitude() + "," + location.getLongitude(),
                settingsIntent.getStringExtra("target"));
        mNav.setOnPathSetListener(this);
        mNav.setPathColor(Color.BLACK, Color.BLUE, Color.GREEN);
        mNav.setPathLineWidth(10);
        mNav.findDirectionsByString(false);
        animateMarker(mMarker, new LatLng(location.getLatitude(), location.getLongitude()), false);

    }

    private Location convertLatLngToLocation(LatLng latLng) {
        Location location = new Location("location");
        location.setLongitude(latLng.longitude);
        location.setLatitude(latLng.latitude);
        return location;

    }

    private float bearingBetweenLatLngs(LatLng beginLatLng,LatLng endLatLng) {
        Location beginLocation = convertLatLngToLocation(beginLatLng);
        Location endLocation = convertLatLngToLocation(endLatLng);
        return beginLocation.bearingTo(endLocation);
    }

    public void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hidingMarker) {
        final LatLng startPosition = marker.getPosition();
        final LatLng finalPosition = toPosition;
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final Interpolator interpolator = new AccelerateDecelerateInterpolator();
        final float durationInMs = 3000;
        final boolean hideMarker = hidingMarker;
        marker.setRotation(bearingBetweenLatLngs(startPosition, finalPosition));

        handler.post(new Runnable() {
            long elapsed;
            float t;
            float v;

            @Override
            public void run() {
                // Calculate progress using interpolator
                elapsed = SystemClock.uptimeMillis() - start;
                t = elapsed / durationInMs;
                v = interpolator.getInterpolation(t);

                LatLng currentPosition = new LatLng(
                        startPosition.latitude*(1-t)+finalPosition.latitude*t,
                        startPosition.longitude*(1-t)+finalPosition.longitude*t);


                marker.setPosition(currentPosition);

                // Repeat till progress is complete.
                if (t < 1) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }

    private void send(JSONObject message) {
        Intent intent = new Intent(getString(R.string.network_service_send));
        intent.putExtra("payload", message.toString());
        sendBroadcast(intent);
    }

    @Override
    public void onPathSetListener(Directions directions) {
        iterator = directions
                .getRoutes()
                .get(0)
                .getLegs()
                .get(0)
                .getSteps()
                .iterator();

        currStep = directions
                .getRoutes()
                .get(0)
                .getLegs()
                .get(0)
                .getSteps()
                .get(0);



        Log.d("Directions", "routes.size is " + directions.getRoutes().size());
        if (directions.getRoutes().get(0) != null) {
            Log.d("Directions", "routes.get(0) isn't null");
        } else {
            Log.d("Directions", "routes.get(0) is" + " null");
        }

        Route currRoute = directions.getRoutes().get(0);
        Legs startLeg = currRoute.getLegs().get(0);
        Steps startStep = startLeg.getSteps().get(0);
        if (startLeg != null && startStep != null) {
            Log.d("Directions", "startLeg and start step isn't null");
            Log.d("Directions", "legs.size = " + currRoute.getLegs().size());
        } else {
            Log.d("Directions", "startLeg and start step is null");
        }
        CameraPosition position = new CameraPosition(startStep.getStepStartPosition(),
                ZOOM,
                TILT,
                bearingBetweenLatLngs(startStep.getStepStartPosition(), startStep.getEndStepPosition()));
        map.moveCamera(CameraUpdateFactory.newCameraPosition(position));
        mMarker = map.addMarker(new MarkerOptions()
                        .position(startStep.getStepStartPosition())
                        .flat(true)
                        .icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.ic_navigation))
                        .draggable(false)
        );
    }
}
