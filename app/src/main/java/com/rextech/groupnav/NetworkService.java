package com.rextech.groupnav;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketException;
import de.tavendo.autobahn.WebSocketHandler;

public class NetworkService extends Service {

    private final String URI = "ws://104.40.184.47:8081//";

    private final String TAG = "Network Service";

    private boolean inGroup = false;



    private WebSocketConnection mConnection;

    public NetworkService() {
    }

    private boolean checkKeys(JSONObject msg, String... keys) {

        Set<String> keySet = new HashSet<>();
        Iterator<String> iterator = msg.keys();

        while (iterator.hasNext()){
            keySet.add(iterator.next());
        }

        for (String key : keys) {
            if ((!keySet.contains(key))) return false;
            else if (msg.isNull(key)) return false;
        }
        return true;
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d(TAG, "Service started");



        mConnection = new WebSocketConnection();

        try {
            mConnection.connect(URI, new WebSocketHandler() {

                @Override
                public void onOpen() {
                    Log.d(TAG, "connection established");
                    super.onOpen();
                }

                @Override
                public void onTextMessage(String payload) {
                    Log.d(TAG, "recieved message : " + payload);

                    JSONObject message = null;
                    try {
                        message = new JSONObject(payload);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (checkKeys(message, "from", "operation")){

                        try {
                            if (message.getString("operation").equals("groupDestroy")){
                                inGroup = false;
                                send(message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (inGroup) {
                            //group operation processing
                        }

                    } else if (checkKeys(message, "result", "offline")) {
                        inGroup = true;
                        //process response from group creating
                    } else if (checkKeys(message, "result")) {
                        //process usually response
                    }





                    super.onTextMessage(payload);
                }


                @Override
                public void onClose(int code, String reason) {
                    Log.d(TAG, "connection closed: reason " + reason);
                    super.onClose(code, reason);
                }
            });
        } catch (WebSocketException e) {
            e.printStackTrace();
        }

        IntentFilter filter = new IntentFilter(getString(R.string.network_service_send));
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra("method")) {

                    if (mConnection.isConnected()) {
                        Log.d(TAG, "Authenticate command evaulated : method -> "
                                + intent.getStringExtra("method")
                                + ", email -> "
                                + intent.getStringExtra("email")
                                + ", password -> "
                                + intent.getStringExtra("password"));
                        JSONObject message = new JSONObject();
                        try {
                            message.put("operation", intent.getStringExtra("method"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            message.put("email", intent.getStringExtra("email"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            message.put("password", intent.getStringExtra("password"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d(TAG, "JSON message created: " + message.toString());
                        mConnection.sendTextMessage(message.toString());
                        return;
                    } else {
                        Log.d(TAG, "AHTUNG!! WEBSOCKET IS NOT CONNECTED");
                        return;
                    }
                }
                Log.d(TAG, "TO SERVER " + intent.getStringExtra("payload"));
                mConnection.sendTextMessage(intent.getStringExtra("payload"));
            }
        }, filter);

        return super.onStartCommand(intent, flags, startId);
    }

    private void send(JSONObject message) {
        Log.d(TAG, "broadcast sending :" + message.toString());
        Intent tIntent = new Intent(getString(R.string.network_service_recieve))
                .putExtra("payload", message.toString());
        sendBroadcast(tIntent);
    }



    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
